package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class Window2 {
    @FXML
    private Label lbWinSec;

    public String getLbText() {
        return lbWinSec.getText();
    }

    public void setLbText(String txt) {
        this.lbWinSec.setText(txt);
    }


}
